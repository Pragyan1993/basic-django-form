from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth import authenticate, login
from .forms import Info
from .models import User_Info
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from phonenumber_field.phonenumber import to_python

def user_info(request):
 if request.method == 'POST':
     form = Info(request.POST)

     if request.POST.get("submit"):

        name = request.POST.get('name', '')
        gender = request.POST.get('gender', '')
        email = request.POST.get('email', '')
        phone_number = request.POST.get('phone_number', '')

        existing_user=User_Info.objects.all()
        for user in existing_user:
            if user.email==email:
                return HttpResponse("The entered email already exists")
        else:
            try:
                validate_email(email)

                user=User_Info.objects.create(name=name,gender=gender,email=email,phone_number=phone_number)
                user.save()
            except ValidationError as e:
                print("It seems that the email is not correct, details:", e)
            return render(request, 'account/redirect.html')
     elif request.POST.get("cancel"):
                form=Info()
                return render(request, 'account/login.html', {'form': form})
 else:
    form = Info()
 return render(request, 'account/login.html', {'form': form})
