from django import forms
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget
#from .models import User_Info
GENDER = (
(None, 'Choose your gender'),
('male', 'male'),
('female', 'female'),
('custom', 'custom'),
('Prefer Not To Say', 'Prefer Not To Say'),
)
class GenderField(forms.ChoiceField):
 def __init__(self, *args, **kwargs):
  super(GenderField, self).__init__(*args, **kwargs)
  self.choices = ((None,'Select gender'),('M','Male'),('F','Female'))
class Info(forms.Form):
 name = forms.CharField()
 gender = forms.ChoiceField(choices=GENDER)
 email= forms.EmailField()
 phone_number = PhoneNumberField(widget=PhoneNumberPrefixWidget(initial='IN'))
