from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

class User_Info(models.Model):
 name = models.CharField(max_length=100)
 gender = models.CharField(max_length=50)
 email= models.EmailField(unique=True)
 phone_number = PhoneNumberField()
 def __str__(self):
  return self.name
